#ifndef BONZAITEST_H
#define BONZAITEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "../src/SluEval.hpp"

class SluEvalTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( SluEvalTest );
  CPPUNIT_TEST( testF1 );
  CPPUNIT_TEST( testnbS );
  CPPUNIT_TEST_SUITE_END();

  SluEval _eval;

public:
    SluEvalTest();
    void testF1() { CPPUNIT_ASSERT_DOUBLES_EQUAL(50.0,_eval.f1(),0.0001);}
    void testnbS() {CPPUNIT_ASSERT_EQUAL(6u,_eval.nbSentences());}
   
    
};

#endif  