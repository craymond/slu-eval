#include "SluComp.hpp"




double SluComp::pvalue(const double v)
{
    const std::map<double,double> studentTable={{0.126,0.9},{0.674,0.5},{1.036,0.3},{1.282,0.2},{1.645,0.1},{1.96,0.05},{2.326,0.02},{2.576,0.01},{3.291,0.001},{std::numeric_limits<double>::max(),0.0}};
    auto sup=std::upper_bound(studentTable.begin(),studentTable.end(),v,[](const double val,const auto& p){return val<p.first;});
    --sup;
    return sup->second;
}


SluComp::SluComp(const std::string& file1,const std::string& file2,const bool cv): sys1(file1,cv),sys2(file2,cv),_f1Diff(sys1.nbSentences()),_werDiff(sys1.nbSentences())
{
    if(sys1.nbSentences()!=sys2.nbSentences())    
        throw std::runtime_error(std::format("SluComp: file1 and file2 do not have the same number of sentences:{}!={}",sys1.nbSentences(),sys2.nbSentences()));
    sys1.classification_report();//force computation
    sys2.classification_report();//force computation
    for(uint i=0;i<sys1.nbSentences();++i)
    {
        //should be positive if sys2>sys1
        _werDiff[i]=sys1.wer(i)-sys2.wer(i);
        _f1Diff[i]=sys2.f1(i)-sys1.f1(i);
        // if(sys1.wer(i)<sys2.wer(i) && sys1.f1(i)<=sys2.f1(i) )
        // {
        // std::cout << "\nsys1:"<<i<<"\n"<<sys1(i);
        // std::cout << "\nsys2:"<<i<<"\n"<<sys2(i);
        // }
    }
    
}

double SluComp::testRes::compute_precise_pvalue() const
{
    const double pas=0.001;
    double som=0;
    for(double i=pas;i<ttest;i+=pas)
    {
        som+=crnl(i)*pas;
    }
    return 2*(0.5-som);
}


void SluComp::comparConceptByConceptMetrics() const
{
    const auto res=sys1.compar(sys2);
    std::cout << "\n"<< res<<"\n";
}

SluComp::testRes SluComp::ttest(const std::vector<double>& difference) const
{
    const double moyenne=std::accumulate(difference.begin(),difference.end(),0.0)/sys1.nbSentences();
    const double variance=std::accumulate(difference.begin(),difference.end(),0.0,[moyenne](const double c,const double e){return c+(e-moyenne)*(e-moyenne);})/(sys1.nbSentences()-1);
    const double erreur_standard=sqrt(variance)/sqrt(difference.size());//moyenne ecart_type
    const double t=erreur_standard>0 ? fabs(moyenne)/erreur_standard : 0;
    //cuint degredeliberte=diff.size()-1;
    
    return {moyenne,t,pvalue(t)};
}

asciitablestream SluComp::testRes::operator()(const StatTest t) const
{
    const std::string& system=mean>0? "sys2":"sys1";
    asciitablestream out(asciitablestream::Alignment::RIGHT,asciitablestream::Style::SIMPLE,4u,false);
    if(pvalue>=1)
        out<<asciitablestream::Alignment::LEFT<<system<<" = sys2"<<asciitablestream::endl;        
    else if(pvalue>0.05)
        out<<asciitablestream::Alignment::LEFT<<system<<" looks better"<<asciitablestream::endl;        
    else
        out<<asciitablestream::Alignment::LEFT<<system<<" is better:"<<asciitablestream::endl;
    
    if(t==StatTest::Wilcoxon)
        out <<asciitablestream::Alignment::RIGHT<<"rank sum balance:"<<mean<<asciitablestream::endl;
    else
        out <<asciitablestream::Alignment::RIGHT<<"mean difference:"<<mean<<asciitablestream::endl;
    out <<"test"<<ttest<<asciitablestream::endl;    
    //out <<"pvalue (min)"<<pvalue<<asciitablestream::endl;
    out <<"pvalue"<<compute_precise_pvalue()<<asciitablestream::endl;
    return out;
    
}

double SluComp::ttest() const
{
    auto f1=ttest(_f1Diff)(StatTest::Student);
    auto wer=ttest(_werDiff)(StatTest::Student);

    const auto& mess1=std::format("paired Student test on F1\nsys1({:.4f}) vs sys2({:.4f})",sys1.f1(),sys2.f1());
    const auto& mess2=std::format("paired Student test on WER\nsys1({:.4f}) vs sys2({:.4f})",sys1.wer(),sys2.wer());

    std::cout << "\n" << f1.toStringBeside(wer,"\t|\t",mess1,mess2);
    
    return 1;
}

asciitablestream SluComp::countLocalImprovments(const std::vector<double>& difference) const
{
    const auto idem=std::count_if(difference.cbegin(),difference.cend(),[](const auto& e){return e==0;});
    const auto sys2better=std::count_if(difference.cbegin(),difference.cend(),[](const auto& e){return e>0;});
    const auto sys1better=std::count_if(difference.cbegin(),difference.cend(),[](const auto& e){return e<0;});
    asciitablestream out(asciitablestream::Alignment::RIGHT,asciitablestream::Style::SIMPLE);
    out << std::format("#Sentences: {}=={}",difference.size(),(idem+sys1better+sys2better))<<"#num"<<"Perc."<<asciitablestream::endl;
    out << "equal: "<<idem<<(idem*100.0/difference.size())<<asciitablestream::endl;
    out << "sys1 is better: "<<sys1better<<sys1better*100.0/difference.size()<<asciitablestream::endl;
    out << "sys2 is better: "<<sys2better<<sys2better*100.0/difference.size()<<asciitablestream::endl;
    return out;
}


std::string SluComp::detailsLocalImprovments(const std::vector<double>& difference) const
{
    std::ostringstream out;
    std::vector<uint> index_to_keep;
    for(uint i=0;i<difference.size();++i)
        if(difference[i]==0)
            index_to_keep.push_back(i);
    SluEval idem(sys1,index_to_keep);
    out << "\n"<<SEPARATOR<<"CASE WHERE SYS ARE EQUIVALENT\n"<<idem.classification_report();

    index_to_keep.clear();
    for(uint i=0;i<difference.size();++i)
         if(difference[i]>0)
             index_to_keep.push_back(i);
    if(!index_to_keep.empty())
    {
        SluEval sys2better1(sys1,index_to_keep);
        SluEval sys2better2(sys2,index_to_keep);
        out << "\n"<<SEPARATOR<<"CASE WHERE SYS2 IS BETTER\n"<<sys2better1.compar(sys2better2);
    }

    index_to_keep.clear();
    for(uint i=0;i<difference.size();++i)
        if(difference[i]<0)
            index_to_keep.push_back(i);
    if(!index_to_keep.empty())
    {
        SluEval sys1better1(sys1,index_to_keep);
        SluEval sys1better2(sys2,index_to_keep);
        out << "\n"<<SEPARATOR<<"CASE WHERE SYS1 IS BETTER\n"<<sys1better1.compar(sys1better2);
    }
    return out.str();
}

void SluComp::countLocalImprovments(const bool details) const
{
    const auto& tcer=countLocalImprovments(_werDiff);
    const auto& tf1=countLocalImprovments(_f1Diff);
    std::cout << tf1.toStringBeside(tcer,"\t|\t","Local improvment in F1","Local improvment in WER");
    if(details)
        std::cout <<"\n"<<detailsLocalImprovments(_f1Diff);
}


SluComp::testRes SluComp::wilcoxon(const std::vector<std::pair<bool,double>>& rank) const
{
   
    std::set<double> vals;
    std::for_each(rank.begin(),rank.end(),[&vals](const auto& e){vals.insert(e.second);});
    std::map<double,double> mean_rank;
    for(const auto val: vals)
    {
        const double rmin=std::find_if(rank.begin(),rank.end(),[val](const auto& p){return p.second==val;})-rank.begin()+1;
        const double rmax=rank.size()-(std::find_if(rank.rbegin(),rank.rend(),[val](const auto& p){return p.second==val;})-rank.rbegin());
        mean_rank[val]=(rmax+rmin)/2.0;
        //std::cerr << val<<" found from"<<rmin<<" to "<<rmax<<"\n";
    }
    
    const double t1=std::accumulate(rank.begin(),rank.end(),0,[&mean_rank](const double c,const auto& e){if(e.first) return c; return c+mean_rank[e.second]; });
    const double t2=std::accumulate(rank.begin(),rank.end(),0,[&mean_rank](const double c,const auto& e){if(e.first) return c+mean_rank[e.second]; return c;  });
    const double mint=std::min(t1,t2);
    const double N=rank.size();
    const double Et=N*(N+1)/4;
    const double St=sqrt(N*(N+1.0)*(2*N+1)/24);
    const double z=St>0 ? fabs((mint-Et)/St): 0;

    const double vote=std::max(t1,t2)*100/(t1+t2);
      
    //std::cout <<"\nr="<<minr<<" z="<< z<<" pvalue="<<pvalue(z)<<"\n";
    return {vote,z,pvalue(z)};
   
}

 double SluComp::wilcoxon() const
 {
    std::vector<std::pair<bool,double>> rank; 

    for(const auto e: _f1Diff)
    {
        if(e>0)
            rank.emplace_back(true,e);
        if(e<0)
            rank.emplace_back(false,fabs(e));
    }
    std::sort(rank.begin(),rank.end(),[](const auto& un,const auto& deux){return un.second<deux.second;});
    auto resf1=wilcoxon(rank);
    
    rank.clear();

    for(const auto e: _werDiff)
    {
        if(e>0)
            rank.emplace_back(true,e);
        if(e<0)
            rank.emplace_back(false,fabs(e));
    }
    std::sort(rank.begin(),rank.end(),[](const auto& un,const auto& deux){return un.second>deux.second;});
    auto reswer=wilcoxon(rank);
    
    
    const auto& mess1=std::format("signed rank wilcoxon on F1\nsys1({:.4f}) vs sys2({:.4f})",sys1.f1(),sys2.f1());
    const auto& mess2=std::format("signed rank wilcoxon on WER\nsys1({:.4f}) vs sys2({:.4f})",sys1.wer(),sys2.wer());
 
    std::cout << resf1(StatTest::Wilcoxon).toStringBeside(reswer(StatTest::Wilcoxon),"\t|\t",mess1,mess2);
    return 0;
 }