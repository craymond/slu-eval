#ifndef SLUEVAL_H
#define SLUEVAL_H
#include "Data.hpp"
#include "Metrics.hpp"
#include <unordered_map>
#include <string>

namespace std {
template <>
class hash<std::pair<std::string,std::string>> 
{
 public:
  size_t operator()(const std::pair<std::string,std::string> &p) const
  {
    return std::hash<std::string>{}(p.first)^std::hash<std::string>{}(p.second);
  }
};
}

struct errors_detail
{
    using pstring=std::pair<std::string,std::string>;
    //frequency of errors per class
    std::unordered_map<std::string,uint> insertions;
    std::unordered_map<std::string,uint> deletions;
    std::unordered_map<std::string,uint> substitutions;
    std::unordered_map<pstring,uint> confusions;
    template<class T>
    static std::vector<std::pair<T,uint>> sorted(const std::unordered_map<T,uint>& ens)
    {
        std::vector<std::pair<T,uint>> enssorted(ens.size());
        std::copy(ens.begin(),ens.end(),enssorted.begin());
        std::sort(enssorted.begin(),enssorted.end(),[](const auto& un,const auto& deux){if(un.second==deux.second) return un.first<deux.first; else return un.second>deux.second; });
        return enssorted;
    }
    template<class T>
    static uint sum(const std::unordered_map<T,uint>& ens)
    {
        return std::accumulate(ens.begin(),ens.end(),0,[](const uint cumul,const auto& e){return cumul+e.second;});
    }
};
class SluEval
{
    using cuint=const unsigned int;
    Data _data;

    std::vector<SequenceAligner<concept_value>::Alignment> _alignment; //result of sentences alignment between ref/reco    
    std::unique_ptr<SequenceAligner<concept_value>::SummaryStatistics> _ss;
    
    mutable std::unique_ptr<metrics::classificationReport> _cr; //classification report
    mutable errors_detail _ed; //errors details

    void compute_details() const;
public:
    SluEval(const std::string& filename,const bool CV=false,const Data::FileType ft=Data::FileType::AUTO);
    SluEval(const SluEval& original,const std::vector<uint>& index_to_keep);
    std::string classification_report() const;
    std::string summaryStatistics() const;
    std::string errorDetail() const;
    auto operator()(cuint i) const {return _alignment[i];}
    double f1() const {return _cr->fscore();}
    double wer() const {return _cr->cer();}
    double f1(cuint i) const;
    double wer(cuint i) const;
    uint nbSentences() const {return _ss->getNumSentences();}
    std::string compar(const SluEval& d,const bool relative_difference=true) const;
    friend std::ostream& operator<<(std::ostream& o,const SluEval& e);

    class Args
    {
        bool _value=false;
        Data::FileType _ftype=Data::FileType::AUTO;
        bool _g=false;
        bool _c=false;
        bool _e=false;
        
        public:
        explicit Args(const std::string& s)
        {
            if(s.find('v')!=std::string::npos) _value=true;
            if(s.find('g')!=std::string::npos) _g=true;
            if(s.find('e')!=std::string::npos) _e=true;
            if(s.find('c')!=std::string::npos) _c=true;
            if(s.find("C")!=std::string::npos) _ftype=Data::FileType::CRF;
            if(s.find("M")!=std::string::npos) _ftype=Data::FileType::MPM;
            if(s.find("L")!=std::string::npos) _ftype=Data::FileType::MPML;
            if(s.find('h')!=std::string::npos)
            {
                help();
                exit(EXIT_SUCCESS);
            }
            if(!_g && !_e && !_c) _g=true;
        }
        bool value() const noexcept {return _value;}
        Data::FileType filetype() const noexcept {return _ftype;}
        bool print_global_metrics() const noexcept {return _g;}
        bool print_concept_metrics() const noexcept {return _c;}
        bool print_errors() const noexcept {return _e;}
        static void help()  
        {
            std::cerr<<"use: slu-eval [gcevCMLh] <file>\n\n";
            std::cerr<<"\t- C\t#input file is using a CRF  file format\n";
            std::cerr<<"\t- L\t#input file is using a MPML file format\n";
            std::cerr<<"\t- M\t#input file is using a MPM  file format\n";
            std::cerr<<"\t- g\t#output global metrics\n";
            std::cerr<<"\t- c\t#output concepts metrics\n";
            std::cerr<<"\t- e\t#output errors\n";        
            std::cerr<<"\t- v\t#flag activate the use of values\n";    
            std::cerr<<"\t- h\t#this help\n\n";
        }

};

};



#endif