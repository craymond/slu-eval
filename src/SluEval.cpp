#include "SluEval.hpp"

void SluEval::compute_details() const
{
    //too soon because we don't know the complete list of concepts if the hyp file contains more concepts than ref file
    _cr=std::make_unique<metrics::classificationReport>(concept_value::nbConcepts());
   
    for (const auto& e: _alignment)
    {
        for(uint i=0;i<e.reference.size();++i)
        {
            if(e.reference[i].empty()) //insertion
            {
                _cr->addMultilabel({},{e.hypothesis[i].concept_idx()});
                ++_ed.insertions[e.hypothesis[i].concept_name()];
            }
            else 
            {
                if(e.hypothesis[i].empty()) //deletion
                {
                    _cr->addMultilabel({e.reference[i].concept_idx()},{});
                    ++_ed.deletions[e.reference[i].concept_name()];
                }
                else //substitutions ou correct
                {
                    
                    if(e.reference[i]!=e.hypothesis[i])
                    {
                        ++_ed.substitutions[e.reference[i].concept_name()];
                        ++_ed.confusions[{e.reference[i].concept_name(),e.hypothesis[i].concept_name()}];
                        //dans le cas ou different: deux possibilités: concept different ou valeur differente
                        if (e.reference[i].concept_idx()==e.hypothesis[i].concept_idx()) //ici concept ok mais valeur different je compte ça comme une deletion + une insertion (car je ne peux pas reporter l'erreur sur un autre concept)
                        {    
                            _cr->addMultilabel({e.reference[i].concept_idx()},{});
                            _cr->addMultilabel({},{e.hypothesis[i].concept_idx()});
                        }
                        else //sinon substitution standard
                            _cr->addMultilabel({e.reference[i].concept_idx()},{e.hypothesis[i].concept_idx()});
                    }
                    else // correct
                        _cr->addMultilabel({e.reference[i].concept_idx()},{e.hypothesis[i].concept_idx()});
                }
            }
            
        }
    }   
    _cr->getAsText();//force computation of metrics
}


std::string SluEval::summaryStatistics() const
{
   
    std::ostringstream out;
    out <<"\nSUMMARY STATISTICS\n"<< *_ss<<"\n";
    return out.str();
}

SluEval::SluEval(const SluEval& original,const std::vector<uint>& index_to_keep)
{
    for(const auto i: index_to_keep)
        _alignment.push_back(original._alignment[i]);
    _ss=std::make_unique<SequenceAligner<concept_value>::SummaryStatistics>(_alignment);
}

SluEval::SluEval(const std::string& filename,const bool CV,const Data::FileType ft):_data(filename,ft)
{
    _alignment=_data.getAlignedConcepts(CV);
    _ss=std::make_unique<SequenceAligner<concept_value>::SummaryStatistics>(_alignment);
    
    /*for(const auto& a : _alignment)
        std::cerr << a << "\n";
    */
    //to do in other place
    //this->compute_details(_alignment);
    // _cr->getAsText();//force computation of metrics
    

    // auto comp=[](const auto& c){return c.concept_name().find("arrive_time.time_relative")!=std::string::npos;};
    // for(const auto& a: _alignment)
    //  if(!a.isSentenceCorrect() && (std::find_if(a.reference.begin(),a.reference.end(),comp)!=a.reference.end() || std::find_if(a.hypothesis.begin(),a.hypothesis.end(),comp)!=a.hypothesis.end()))
    //     std::cerr << a;

}

std::ostream& operator<<(std::ostream& o,const SluEval& e)
{
    if(!e._cr) e.compute_details();
    o<<e.classification_report()<<"\n";
    o<<e.summaryStatistics()<<"\n";
    o<<e.errorDetail()<<"\n";
    return o;
}

std::string SluEval::classification_report() const 
{
    if(!_cr) compute_details();
    return  "\n\nCLASSIFICATION REPORT\n"+_cr->getAsText(concept_value::labels_name())+"\n";
}

std::string SluEval::errorDetail() const
{
    if(!_cr) compute_details();
    asciitablestream conf(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,2,false);
    conf<< "Substituted"<<"by"<<errors_detail::sum(_ed.confusions)<<asciitablestream::endl;
    for(const auto &[tag_pred,nb]: errors_detail::sorted(_ed.confusions))
         conf << tag_pred.first<<tag_pred.second<<nb<<asciitablestream::endl;
    asciitablestream subs(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,2,false);
    subs<< "Substitutions "<<errors_detail::sum(_ed.substitutions)<<asciitablestream::endl;
    for(const auto &[tag,nb]: errors_detail::sorted(_ed.substitutions))
          subs <<tag<<nb<<asciitablestream::endl;
    asciitablestream ins(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,2,false);
    ins<< "Insertions "<<errors_detail::sum(_ed.insertions)<<asciitablestream::endl;
    for(const auto &[tag,nb]: errors_detail::sorted(_ed.insertions))
          ins << tag<<nb<<asciitablestream::endl;
    asciitablestream del(asciitablestream::Alignment::LEFT,asciitablestream::Style::LIGHT,2,false);
    del<< "Deletions  "<<errors_detail::sum(_ed.deletions)<<asciitablestream::endl;
    for(const auto &[tag,nb]: errors_detail::sorted(_ed.deletions))
          del << tag<<nb<<asciitablestream::endl;
    return "\n\nERRORS DETAILS\n"+conf.toString()+subs.toString()+ins.toString()+del.toString();
}

double SluEval::f1(cuint i) const
{
  return _alignment[i].f1();
}

double SluEval::wer(cuint i) const
{
  return _alignment[i].wer();
}


std::string SluEval::compar(const SluEval& d,const bool relative_difference) const
{
    if(!_cr) compute_details();
    if(!d._cr) d.compute_details();

    return _cr->compar(*d._cr,relative_difference,concept_value::labels_name());
}
