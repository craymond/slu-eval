 #include "Data.hpp"


//init static mapping int2name
std::map<std::string,uint> concept_value::_concept2idx; 
std::vector<std::string> concept_value::_idx2concept;
bool concept_value::_compareCV=false;


uint concept_value::concept2idx(const std::string& n)
{ 
     auto it=_concept2idx.find(n); if(it==_concept2idx.end()){ _concept2idx[n]=_concept2idx.size();_idx2concept.push_back(n);} return _concept2idx.at(n); 
}

concept_value::concept_value(const std::string& c,const std::string& v): _concept(concept2idx(c)),_value(v)
{

}

std::string concept_value::concept_name() const 
{
    if(_value.empty()) 
        return "*"; 
     if (concept_value::_compareCV) 
        return _idx2concept[_concept]+"["+_value+"]";
    return _idx2concept[_concept];
}

uint concept_value::concept_idx() const 
{   
    if(_value.empty()) 
        return _idx2concept.size(); 
    else 
        return _concept;
}
uint concept_value::concept_value::size() const 
{
    if(_value.empty()) return 1;
    if (_compareCV) return _idx2concept[_concept].size()+_value.size()+2;
    else return _idx2concept[_concept].size();
}

bool concept_value::operator==(const concept_value& cv) const
{
    if(_compareCV) return this->_concept==cv._concept && this->_value==cv._value;
    else return this->_concept==cv._concept;
}
bool concept_value::operator!=(const concept_value& cv) const
{
    return !(*this==cv);
}

int concept_value::operator-(const concept_value& cv) const
{
    if(this->_concept==cv._concept) return 50; //si les noms de concepts correspondent on facilite l'alignement
    //if( this->_value==cv._value) return 50; //on pourrait faire pareil pour les valeurs
    return 0;
}

std::ostream& operator<<(std::ostream& o,const concept_value& cv)
{
    o << cv.concept_name();
    //if (concept_value::_compareCV) o << "["<<cv._value<<"]";
    return o;
}

 Data::FileType Data::determine_filetype(const std::string& file) const
 {
     if(file.find(".MPML",file.size()-5)!=std::string::npos) return FileType::MPML;
     if(file.find(".mpml",file.size()-5)!=std::string::npos) return FileType::MPML;
     if(file.find(".mpm",file.size()-4)!=std::string::npos) return FileType::MPM;
     if(file.find(".MPM",file.size()-4)!=std::string::npos) return FileType::MPM;    
     if(file.find(".crf",file.size()-4)!=std::string::npos) return FileType::CRF;
     if(file.find(".CRF",file.size()-4)!=std::string::npos) return FileType::CRF;
     throw std::runtime_error("Can not determine automatically file type, please use .mpm,.mpml or .crf extension or explicity tell to slu-eval the type of your file");
 }

void Data::process_mpml_line(const std::string& sline)
 {
    std::istringstream line(sline);
    std::string token;
    line >> token;
    if(token.find("REF")==std::string::npos && token.find("HYP")==std::string::npos)
        throw std::runtime_error("Line \""+sline+"\" is not valid, it should starts by \"REF\" or \"HYP\"");
    cv_seq_list& slist= token.find("REF")!=std::string::npos ? _cv_ref: _cv_reco;
    while(line>>token)
    {
         slist.back().emplace_back(token,"-");//value non-empty to avoid concept_value interpreted as invalid
    }
 }

 void Data::process_mpm_line(const std::string& sline)
 {
    std::istringstream line(sline);
    std::string token;
    bool inside=false;
    line >> token;
    if(token.find("REF")==std::string::npos && token.find("HYP")==std::string::npos)
        throw std::runtime_error("Line \""+sline+"\" is not valid, it should starts by \"REF\" or \"HYP\"");
    cv_seq_list& slist= token.find("REF")!=std::string::npos ? _cv_ref: _cv_reco;
    while(line>>token)
    {
        if(token.size()>=2 && token[0]=='<' && token[1]=='/')
           inside=false;
        else
        {
            if(token[0]=='<')
            {
               inside=true;
               slist.back().emplace_back(token.substr(1,token.size()-2),"");
            }
            else
                if (inside)
                    slist.back().back().empty() ? slist.back().back()._value+=token: slist.back().back()._value+=" "+token;
        }
    }
 }

void Data::load_mpm(std::istream& file)
{
    std::string id,line;
    while(getline(file,id))
    {
        getline(file,line);process_mpm_line(line);
        getline(file,line);process_mpm_line(line);
        getline(file,line);//should be empty line
        _cv_reco.resize(_cv_reco.size()+1);
        _cv_ref.resize(_cv_ref.size()+1); 
    }
    //remove obviously empty last entry
    _cv_reco.pop_back();
    _cv_ref.pop_back(); 
}

void Data::load_mpml(std::istream& file)
{
    std::string id,line;
    while(getline(file,id))
    {
        getline(file,line);process_mpml_line(line);
        getline(file,line);process_mpml_line(line);
        getline(file,line);//should be empty line
        _cv_reco.resize(_cv_reco.size()+1);
        _cv_ref.resize(_cv_ref.size()+1); 
    }
    //remove obviously empty last entry
    _cv_reco.pop_back();
    _cv_ref.pop_back();   

}

void Data::load_crf(std::istream& file)
{
    static const std::regex valid_line("^(\\S+.*\\s+([BbiI]\\-\\S+|\\S+\\-[BbiI]|O)\\s+([BbiI]\\-\\S+|\\S+\\-[BbiI]|O)\\s*)|(\\s*)$");
    static const std::regex pline("^(\\S+).*\\s+(\\S+)\\s+(\\S+)\\s*$"); //R"^(\S+).*\s+(\S+)\s+(\S+)\s*$"
    static const std::regex start_concept("^[Bb]\\-(\\S+)|(\\S+)\\-[Bb]$");
    static const std::regex inside_concept("^[iI]\\-(\\S+)|(\\S+)\\-[iI]$");
    std::string line;
    std::smatch m;
    std::smatch c;
    bool previous_line_empty=false;
    for(uint i=1;getline(file,line);++i)
    {
        if(!std::regex_match(line,valid_line)) throw std::runtime_error("Line "+std::to_string(i)+" is not valid:\""+line+"\"");
        
        if(std::regex_search (line,m,pline))
        {
            const std::string& mot=m[1];
            const std::string& ref=m[2];
            const std::string& pred=m[3];
           
            //control if reference is not bad formed: a tag concept start by I
            if(std::regex_search(ref,c,inside_concept) && (_cv_ref.back().empty() || !(_cv_ref.back().back()==concept_value(c[1].str()+c[2].str(),""))))
            {
                throw std::runtime_error("Line "+std::to_string(i)+" is not valid:\""+line+"\" because the reference concept start by I"); 
            }
           
            if(std::regex_search(ref,c,start_concept))//new concept that start here            
                _cv_ref.back().emplace_back(c[1].str()+c[2].str(),"");                         

            if(std::regex_search(pred,c,start_concept))//new concept that start here             
                _cv_reco.back().emplace_back(c[1].str()+c[2].str(),"");           
            else
            {
                if(std::regex_search(pred,c,inside_concept))//case when the prediction is illformed and start by a I-concept
                    if(_cv_reco.back().empty() || !(_cv_reco.back().back()==concept_value(c[1].str()+c[2].str(),"")))
                        _cv_reco.back().emplace_back(c[1].str()+c[2].str(),"");
            }
            
            if(ref!="O")
                _cv_ref.back().back().empty() ? _cv_ref.back().back()._value+=mot: _cv_ref.back().back()._value+=" "+mot;
            if(pred!="O")
                 _cv_reco.back().back().empty() ? _cv_reco.back().back()._value+=mot: _cv_reco.back().back()._value+=" "+mot;
            previous_line_empty=false;
            
        }
        else//should be empty line
        {
            if(!previous_line_empty)//to deal with many empty lines
            {
                _cv_reco.resize(_cv_reco.size()+1);
                _cv_ref.resize(_cv_ref.size()+1); 
            }
            previous_line_empty=true;          
        }        
        
    }
    //last entry is due to final empty line
    if(previous_line_empty)
    {
    _cv_reco.pop_back();
    _cv_ref.pop_back();
    }
}

void Data::load(const std::string& fname,const FileType f)
{
    _cv_ref.resize(1);
    _cv_reco.resize(1);
    const FileType real= f==FileType::AUTO? determine_filetype(fname): f;
    std::ifstream file(fname);
    if(!file)
        throw std::ifstream::failure("can't open "+fname);
    switch(real)
    {
        case FileType::CRF:load_crf(file); break;
        case FileType::MPM:load_mpm(file); break;
        case FileType::MPML:load_mpml(file); break;
        default: throw std::runtime_error("Can not determine automatically file type, plesae use .mpm,.mpml or .crf extension or explicity tell to slu-eval the type of your file");
    }
}

Data::Data(const std::string& fname,const FileType f)
{
    load(fname,f);
}

std::vector<SequenceAligner<concept_value>::Alignment> Data::getAlignedConcepts(const bool CV) const
{
    concept_value::compareCV(CV);
    SequenceAligner<concept_value> sa;
    //SequenceAligner<concept_value> sa(false,1,50);//substitution penalty to 50 instead of 100
    return sa.align(_cv_ref,_cv_reco);
}