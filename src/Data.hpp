#ifndef DATA_H
#define DATA_H
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <exception>
#include <regex>
#include <iostream>
#include <map>
#include "SequenceAligner.hpp"

using cuint=const unsigned int;
using uint = unsigned int;

class concept_value
{
    uint _concept; //uint because we need to number them for classification report
    static std::map<std::string,uint> _concept2idx;
    static std::vector<std::string> _idx2concept;
    static bool _compareCV;
    
    public:
    std::string _value;  //no need to be private here

    static uint concept2idx(const std::string& n);
    static uint nbConcepts() {return _concept2idx.size();}
    static const std::vector<std::string>& labels_name() {return _idx2concept;}
    static void compareCV(const bool cv){_compareCV=cv;}

    concept_value()=default; //the empty value play the role of invalid object
    concept_value(const std::string& c,const std::string& v);
    std::string concept_name() const;
    uint concept_idx() const;
    uint size() const;
    bool empty() const {return _value.empty();} //can't use _concept because an int and no need to
    bool operator==(const concept_value& cv) const;
    bool operator!=(const concept_value& cv) const;
    int operator-(const concept_value& c) const;
    friend std::ostream& operator<<(std::ostream& o,const concept_value& cv);
    
};



class Data
{
  public: 
    enum class FileType {CRF,MPM,MPML,AUTO};
  private:
    using cuint=const unsigned int;
    using cv_seq_list=std::vector<std::vector<concept_value>>;
    cv_seq_list _cv_ref;
    cv_seq_list _cv_reco;
    
    uint nbLabels() const {return concept_value::nbConcepts();}
    void load_crf(std::istream& file);
    void load_mpm(std::istream& file);
    void load_mpml(std::istream& file);
    void process_mpm_line(const std::string& line);
    void process_mpml_line(const std::string& line);
    FileType determine_filetype(const std::string& file) const;
   
  public:
    Data()=default;
    Data(const std::string& fname,const FileType f=FileType::AUTO);
    void load(const std::string& fname,const FileType f=FileType::AUTO);
    std::vector<SequenceAligner<concept_value>::Alignment> getAlignedConcepts(const bool CV=false) const;
};
#endif