#ifndef SLUCOMP_H
#define SLUCOMP_H
#include "SluEval.hpp"
#include <vector>
#include <set>
#include <iostream>
#include <format>

class SluComp
{
    using cuint=const unsigned int;
    using cstring=const std::string;
    static constexpr const char* const SEPARATOR="__________________________________________________________________________________________\n";
    enum class StatTest {Student,Wilcoxon};
    class testRes
    {
        double compute_precise_pvalue() const;
        static double crnl(const double x) {return 1.0/sqrt(2*M_PI)*exp(-0.5*x*x);}
        public:
        const double mean;
        const double ttest;
        const double pvalue;
        testRes(const double m,const double t,const double p): mean(m),ttest(t),pvalue(p){}
        asciitablestream operator()(const StatTest t) const;
        
    };
  
    static double pvalue(const double val);
 
    //WARNING 2 classificationReport may have different index for labels, because of the concept_value
    //en fait probablement pas car l'index de concept_value est static donc commun aux deux instances de SLUEVAL
    SluEval sys1;
    SluEval sys2;
    std::vector<double> _f1Diff;
    std::vector<double> _werDiff;
    
    testRes ttest(const std::vector<double>&) const;
    testRes wilcoxon(const std::vector<std::pair<bool,double>>& rank) const;
    asciitablestream countLocalImprovments(const std::vector<double>& difference) const;
    std::string detailsLocalImprovments(const std::vector<double>& difference) const;
public:
    SluComp(cstring& file1,cstring& file2,const bool cv=false);

    void comparConceptByConceptMetrics() const;
    void countLocalImprovments(const bool details=false) const;
    double wilcoxon() const;
    double ttest() const;

    class Args
    {
        bool _g=false;
        bool _s=false;
        bool _d=false;
        
        public:
        explicit Args(const std::string& s)
        {
            if(s.find('g')!=std::string::npos) _g=true;
            if(s.find('d')!=std::string::npos) {_d=true; _g=true;}
            if(s.find('s')!=std::string::npos) _s=true;       
            if(s.find('h')!=std::string::npos)
            {
                help();
                exit(1);
            }
            if(!_g && !_s) _g=true;
        }
    
        bool general_comp() const noexcept {return _g;}
        bool details_comp() const noexcept {return _d;}
        bool stat_comp() const noexcept {return _s;}   
        static void help()  
        {
            std::cerr<<"use: slu-comp [gdsh] <sys1> <sys2>\n\n";
            std::cerr<<"\t- g\t#output general comparison\n";
            std::cerr<<"\t- d\t#output details about differences\n";
            std::cerr<<"\t- s\t#output statistics comparison\n";        
            std::cerr<<"\t- h\t#this help\n\n";
        }

};


};

#endif
