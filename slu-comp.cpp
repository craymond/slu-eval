#include "src/SluComp.hpp"




int main(int argc, char *argv[])
{        
    if(argc<3 || argc>4)
    {
        SluComp::Args::help();
        return 1;
    }
    const std::string file2=argv[argc-1];
    const std::string file1=argv[argc-2];
    std::string options("g");
    if(argc>3) options=argv[1];
   
    SluComp::Args arg(options);
    SluComp comparator(file1,file2,false);
    
    std::cout << "\n\n";
    
    if(arg.general_comp())
    {
        comparator.countLocalImprovments(arg.details_comp());
        comparator.comparConceptByConceptMetrics();
    }
    
    if(arg.stat_comp())
    {
        comparator.ttest();
        comparator.wilcoxon();
    }

    return 0;
}