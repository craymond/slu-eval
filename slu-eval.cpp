#include "src/SluEval.hpp"



int main(int argc, char *argv[])
{        
    if(argc<2 || argc>3)
    {
        SluEval::Args::help();
        return 1;
    }
    const std::string file=argv[argc-1];
    std::string options("g");
    if(argc>2) options=argv[1];
   
    SluEval::Args arg(options);

    SluEval eval(file,arg.value(),arg.filetype());
    if(arg.print_global_metrics())
        std::cout <<"\n"<< eval.summaryStatistics()<<"\n";
    if(arg.print_concept_metrics())
        std::cout << eval.classification_report();
    if(arg.print_errors())
        std::cout << eval.errorDetail()<<"\n";
    return 0;
}